# vcpkg_test

Try out [vcpkg][], and follow the example
[Installing and Using Packages Example: SQLite](https://learn.microsoft.com/en-us/vcpkg/examples/installing-and-using-packages).

## Prerequisites

You need these tools to build this project.

- [vcpkg][]
- [CMake][]
- [ninja][]

## 🔨 How to build?

```sh
git clone https://gitlab.com/jcs-workspace/vcpkg_test.git

cd vcpkg_test

vcpkg install sqlite3:x64-windows

cmake -S . -B build/windows/ "-DCMAKE_TOOLCHAIN_FILE=path\to\vcpkg\scripts\buildsystems\vcpkg.cmake"
```

See [scripts/build.bat](scripts/build.bat) for command information!

[vcpkg]: https://vcpkg.io/en/
[CMake]: https://cmake.org/download/
[ninja]: https://github.com/ninja-build/ninja
