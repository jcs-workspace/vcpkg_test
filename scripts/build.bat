@echo off
:: ========================================================================
:: $File: build.bat $
:: $Date: 2023-04-11 14:08:25 $
:: $Revision: $
:: $Creator:  $
:: $Notice: See LICENSE.txt for modification and distribution information
::                   Copyright © 2023 by Shen, Jen-Chieh $
:: ========================================================================

::set VCPKG_ROOT=D:\Users\jshen3\Downloads\Program Files\vcpkg\
set VCPKG_ROOT=D:\Program Files\vcpkg\

set PATH=%PATH%;%VCPKG_ROOT%
set PATH=%PATH%;D:\Users\jshen3\Downloads\Program Files\Emacs\bin\
set PATH=%PATH%;D:\Users\jshen3\Downloads\Program Files\CMake\bin\

cd ../

echo ::Build ninja
cmake -S . -B build/ninja/ -GNinja

echo ::Gathering compilation database information!
copy build\ninja\compile_commands.json .

echo ::Build MSVC solution
cmake -S . -B build/windows/
