/**
 * $File: main.cpp $
 * $Date: 2023-04-11 14:30:42 $
 * $Revision: $
 * $Creator:  $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2023 by Shen, Jen-Chieh $
 */
#include <sqlite3.h>
#include <stdio.h>

int main()
{
    printf("%s\n", sqlite3_libversion());
    return 0;
}
